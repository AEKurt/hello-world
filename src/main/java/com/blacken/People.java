package com.blacken;

public class People {
    String name;
    String surname;
    String propery;

    public People(String name, String surname, String propery) {
        this.name = name;
        this.surname = surname;
        this.propery = propery;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", propery='" + propery + '\'' +
                '}';
    }


}
